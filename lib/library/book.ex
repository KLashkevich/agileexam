defmodule Library.Book do
  use Ecto.Schema
  import Ecto.Changeset

  @derive {Poison.Encoder, only: [:id, :code, :due_date, :start_date, :times_extended, :title]}
  schema "books" do
    field :code, :string
    field :due_date, :string
    field :start_date, :string
    field :times_extended, :integer
    field :title, :string

    timestamps()
  end

  @doc false
  def changeset(book, attrs) do
    book
    |> cast(attrs, [:title, :code, :start_date, :due_date, :times_extended])
    |> validate_required([:title, :code, :start_date, :due_date, :times_extended])
  end
end
