defmodule LibraryWeb.API.LibraryController do
    use LibraryWeb, :controller

    import Ecto.Changeset
    alias Library.{Repo, Book}
    import Ecto.Query, only: [from: 2]

    def get_books(conn, _params) do
        #books = Repo.all(Book)

        query = from book in Book,
                order_by: [asc: book.id],
                select: book

        books = Repo.all(query)

        put_status(conn, 201)
            |> json(%{books: books})
    end

    def extend_book(conn, %{"params" => %{"book_id" => book_id}}) do
        case Repo.get_by(Book, id: book_id) do 
            nil -> put_status(conn, 400)
                |> json(%{error: "Book not found"})
            record -> 
                    case record.times_extended < 4 do
                        true -> 
                            {_,due_date} = Date.from_iso8601(record.due_date)
                            days = case record.code do
                                "A" -> 7
                                "B" -> 14
                                "C" -> 21
                            end
                            new_due_date = Date.to_iso8601(Date.add(due_date, days))
                            new_times_extended = record.times_extended + 1
                            Repo.update(change record, %{due_date: new_due_date, times_extended: new_times_extended})
                            put_status(conn, 201) |> json(%{msg: "The book loan has been extended"})
                        false -> put_status(conn, 400) |> json(%{error: "The extension has been rejected"})
                    end
        end
    end
end