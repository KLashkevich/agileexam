alias Library.Repo
alias Library.Book

Repo.insert!(%Book{title: "Programming Phoenix 1.3", code: "A", start_date: "2019-01-09", due_date: "2019-01-16", times_extended: 0})
Repo.insert!(%Book{title: "Secrets of JavaScript Ninja", code: "B", start_date: "2018-12-19", due_date: "2019-01-16", times_extended: 4})
Repo.insert!(%Book{title: "OOP Patterns", code: "C", start_date: "2019-09-01", due_date: "2019-01-16", times_extended: 2})
Repo.insert!(%Book{title: "The Little Prince", code: "A", start_date: "2018-09-01", due_date: "2018-01-16", times_extended: 2})
Repo.insert!(%Book{title: "The Two Rowers", code: "B", start_date: "2018-12-01", due_date: "2019-01-01", times_extended: 0})