defmodule WhiteBreadContext do
  use WhiteBread.Context
  use Hound.Helpers

  alias Library.{Repo,Book}

  feature_starting_state fn  ->
    Application.ensure_all_started(:hound)
    %{}
  end
  scenario_starting_state fn state ->
    Hound.start_session
    Ecto.Adapters.SQL.Sandbox.checkout(Library.Repo)
    Ecto.Adapters.SQL.Sandbox.mode(Library.Repo, {:shared, self()})
    %{}
  end
  scenario_finalize fn _status, _state -> 
    Ecto.Adapters.SQL.Sandbox.checkin(Library.Repo)
    #Hound.end_session
    nil
  end

  given_ ~r/^the following book loans are existing$/, fn state, %{table_data: table} ->
    table
    |> Enum.map(fn book -> Book.changeset(%Book{}, book) end)
    |> Enum.each(fn changeset -> Repo.insert!(changeset) end)
    {:ok, state}
  end

  and_ ~r/^I want to extend item "(?<item_number>[^"]+)"$/,
  fn state, %{item_number: item_number} ->
    {:ok, state |> Map.put(:item_number, item_number)}
  end

  and_ ~r/^I open the web page$/, fn state ->
    navigate_to "/"
    {:ok, state}
  end

  when_ ~r/^I submit item extension$/, fn state ->
    click({:id, ("app-item-button" <> state[:item_number])})
    {:ok, state}
  end

  then_ ~r/^The extension should be updated$/, fn state ->
    :timer.sleep(500)
    assert visible_in_page?(~r/The book loan has been extended/)
    {:ok, state}
  end

  then_ ~r/^The extension should be rejected$/, fn state ->
    :timer.sleep(500)
    assert visible_in_page?(~r/The extension has been rejected/)
    {:ok, state}
  end
end
