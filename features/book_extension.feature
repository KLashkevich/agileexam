Feature: Extend Book
  As a user
  Such that can use borrowed books longer
  I want to extend book loans

  Scenario: Successfull Extension
    Given the following book loans are existing
          | title                       | code | start_date | due_date   | times_extended |
          | Programming Phoenix 1.3     | A    | 2019-01-09 | 2019-01-16 | 0              |
          | Secrets of JavaScript Ninja | B    | 2018-12-19 | 2019-01-16 | 4              |
          | OOP Patterns                | C    | 2019-09-01 | 2019-01-16 | 2              |
    And I want to extend item "1"
    And I open the web page
    When I submit item extension
    Then The extension should be updated

  Scenario: Rejected Extension
    Given the following book loans are existing
          | title                       | code | start_date | due_date   | times_extended |
          | Programming Phoenix 1.3     | A    | 2019-01-09 | 2019-01-16 | 0              |
          | Secrets of JavaScript Ninja | B    | 2018-12-19 | 2019-01-16 | 4              |
          | OOP Patterns                | C    | 2019-09-01 | 2019-01-16 | 2              |
    And I want to extend item "2"
    And I open the web page
    When I submit item extension
    Then The extension should be rejected