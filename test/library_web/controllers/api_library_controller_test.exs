defmodule LibraryWeb.API.LibraryControllerTest do
    use LibraryWeb.ConnCase

    alias Library.{Repo, Book}

    test "GET /api/books", %{conn: conn} do
      Repo.insert!(%Book{title: "Book1", code: "A", start_date: "2019-01-09", due_date: "2019-01-16", times_extended: 0})
      Repo.insert!(%Book{title: "Book2", code: "B", start_date: "2018-12-19", due_date: "2019-01-16", times_extended: 4})
      response =
          conn
          |> get(library_path(conn, :get_books))
          |> json_response(201)

      expected = 2
      assert length(Map.get(response, "books")) == expected
    end

    test "POST /api/books/extend success", %{conn: conn} do
        book_id = Repo.insert!(%Book{title: "Book1", code: "A", start_date: "2019-01-09", due_date: "2019-01-16", times_extended: 0}).id

        request = %{"params" => %{"book_id" => book_id}}

        response =
            conn
            |> post(library_path(conn, :extend_book, request))
            |> json_response(201)
        
        expected = %{"msg" => "The book loan has been extended"}
        assert response == expected
    end

    test "POST /api/books/extend success date update", %{conn: conn} do
        book_id = Repo.insert!(%Book{title: "Book1", code: "A", start_date: "2019-01-09", due_date: "2019-01-10", times_extended: 0}).id

        request = %{"params" => %{"book_id" => book_id}}

        response =
            conn
            |> post(library_path(conn, :extend_book, request))
            |> json_response(201)
        
        expected = "2019-01-17"

        case Repo.get_by(Book, id: book_id) do 
            nil -> assert false
            record -> assert record.due_date == expected
        end
    end

    test "POST /api/items/update failure", %{conn: conn} do
        book_id = Repo.insert!(%Book{title: "Book2", code: "B", start_date: "2018-12-19", due_date: "2019-01-16", times_extended: 4}).id
  
        request = %{"params" => %{"book_id" => book_id}}

        response =
            conn
            |> post(library_path(conn, :extend_book, request))
            |> json_response(400)
    
        expected = %{"error" => "The extension has been rejected"}
        assert response == expected
    end
  end